from discord.ext import commands
from discord.utils import get

bot = commands.Bot(command_prefix='!')
authoritative_roles = ["Admin", "Bots", "Moderator", "Hacker", "Developer", "Music-Bots", "Mechatronics", "Scientist"]
all_roles = ["Admin", "Bots", "Moderator", "Hacker", "Developer", "Music-Bots", "Mechatronics", "Scientist",
             "Python", "C", "C++", "HTML", "CSS", "JavaScript", "Gamer", "GameDeveloper"]


@bot.command()
async def add_role(ctx, arg):
    try:
        user = ctx.message.author
        print(ctx.message.channel.name)
        if ctx.message.channel.name == "request-role":
            if arg in all_roles:
                current_user_roles = [role.name for role in user.roles]
                if arg not in authoritative_roles:
                    if arg in current_user_roles:
                        await ctx.send(f"Role {arg} already assigned to {user}")
                    else:
                        await user.add_roles(get(user.guild.roles, name=arg))
                        await ctx.send(f"Role \"{arg}\" added to {user}")
                else:
                    await ctx.send(f"Role \"{arg}\" is an authoritative role and can not be assigned to you!")
            else:
                await ctx.send(f"Role \"{arg}\" could not be found!")
        else:
            pass

    except Exception as error:
        print(error)
        await ctx.send("Oops, something went wrong. Please try again ...")


@bot.command()
async def remove_role(ctx, arg):
    try:
        user = ctx.message.author
        if ctx.message.channel.name == "request-role":
            if arg in all_roles:
                current_user_roles = [role.name for role in user.roles]
                if arg in current_user_roles:
                    await user.remove_roles(get(user.guild.roles, name=arg))
                    await ctx.send(f"Role \"{arg}\" remove from {user}")
                else:
                    await ctx.send(f"Role \"{arg}\" is not assigned to {user}")
        else:
            pass

    except Exception as error:
        print(error)
        await ctx.send("Oops, something went wrong. Please try again ...")


bot.run("TOKEN")